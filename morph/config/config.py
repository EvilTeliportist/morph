from morph.languages.languages import *
import re

FILE_EXTENSION = ".morph"
VALID_LANGUAGES = {"Python": Python(), "Java": Java()}

# All supported variable types
variableTypes = ["int", "string", "float", "bool"]

# Allowed variable names
ALLOWED_VARIABLE_NAMES = "[a-zA-Z_][a-zA-Z0-9_]*"