from morph.config.config import ALLOWED_VARIABLE_NAMES


CLASS_TAG = "class"
FUNCTION_TAG = "function"
RETURN_TAG = "return"

IF_TAG = "if"
ELSE_TAG = "else"
ELIF_TAG = "elif"

FOR_LOOP_TAG = "for"
WHILE_LOOP_TAG = "while"
CONTINUE_TAG = "continue"
BREAK_TAG = "break"

TRY_TAG = "try"
CATCH_TAG = "catch"
FINALLY_TAG = "finally"

ARRAY_TAG = "array"
ARRAY_INDEX_TAG = r"[a-zA-Z_][a-zA-Z0-9_]*\[([a-zA-Z_][a-zA-Z0-9_]*|\d*)\]"
DICTIONARY_TAG = "dict"
DICTIONARY_INDEX_TAG = r"[a-zA-Z_][a-zA-Z0-9_]*\[\".*\"\]"