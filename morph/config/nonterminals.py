# All nodes that cannot be independent: aka non-terminals.
# For example, all else and elif statments must be eaten by an if statment.
# In this case, if the encoder ever sees an else/elif statement, it should throw
# an error.
NONTERMINAL_NODES = ["else", "elif", "catch", "finally"]
