from morph.languages.languages import AbstractLanguage
from morph.encodings.ASTNode import ASTNode


class Tag(ASTNode):
    pass


class LanguageTag(Tag):
    def __init__(self, language: AbstractLanguage) -> None:
        self.language = language

    def __str__(self) -> str:
        return f"[Language Tag]: {self.language}"

    def __python__(self, indent) -> str:
        return ""

    def __java__(self, indent) -> str:
        return ""
