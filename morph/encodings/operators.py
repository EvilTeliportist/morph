from morph.encodings.loops import Break
from morph.encodings.ASTNode import ASTNode


class Operator(ASTNode):
    """
    Abstract AST node class for operators. The class variables here
    are set to unreachable values because this class should never
    be seen in the AST.
    """

    symbol = "OPERATOR"
    rexp = "$^"  # Matches nothing


# ALL BASIC OPERATORS THAT TAKE AN INPUT ON BOTH SIDES: ie "x + y"
class BasicOperator(Operator):
    """
    Abstract AST node class for basic operators like addition, subtraction
    etc. Takes a left and right side, child classes only have to specify
    a regex expression to match and a symbol to output (like '+' or '-')
    """

    def __init__(self, left: ASTNode, right: ASTNode) -> None:
        self.right = right
        self.left = left

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.left.__python__(0)} {self.symbol} {self.right.__python__(0)}"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"{self.left.__java__(0)} {self.symbol} {self.right.__java__(0)}"

class Add(BasicOperator):
    rexp = r"^\+$"
    symbol = "+"

class Subtract(BasicOperator):
    rexp = r"^\-$"
    symbol = "-"

class Multiply(BasicOperator):
    rexp = r"^\*$"
    symbol = "*"

class Divide(BasicOperator):
    rexp = r"^\/$"
    symbol = "/"

class Modulo(BasicOperator):
    rexp = r"^%$"
    symbol = "%"

class Negate(BasicOperator):

    def __init__(self, value: ASTNode) -> None:
        self.value = value

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"-({self.value.__python__(0)})"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"-({self.value.__java__(0)})"
        

# SPECIAL OPERATORS LIKE INCREMENT AND DECREMENT
class XCrementOperator(Operator):
    """
    Abstract AST node class for custom increment and decrement
    operations, like x-- and x++.
    """
    pass


class PostCrementOperator(XCrementOperator):
    """
    Abstract AST node class for operators where the symbols
    come after the variable. (ie: x++ instead of ++x)
    """
    
    def __init__(self, varname: str) -> None:
        self.varname = varname[:-2]  # Remove last two chars (-- or ++)

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.varname}{self.symbol}"


class PostIncrement(PostCrementOperator):
    symbol = "++"
    rexp = r"^\w+\+\+$"

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.varname} += 1"


class PostDecrement(PostCrementOperator):
    symbol = "--"
    rexp = r"^\w+\-\-"

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.varname} -= 1"


class PreCrementOperator(XCrementOperator):
    """
    Abstract AST node class for operators where the symbols
    come before the variable. (ie: ++x instead of x++)
    """

    def __init__(self, varname: str) -> None:
        self.varname = varname[2:]  # Remove first two chars (-- or ++)

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.symbol}{self.varname}"


class PreIncrement(PreCrementOperator):
    symbol = "++"
    rexp = r"^\+\+\w+$"

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.varname} += 1"


class PreDecrement(PreCrementOperator):
    symbol = "--"
    rexp = r"^\-\-\w+$"

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.varname} -= 1"
