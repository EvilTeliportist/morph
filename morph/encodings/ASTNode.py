from typing import Union
from morph.languages.languages import *
from morph.encodings.dependencies.base import Module


class ASTNode:
    """
    Root class for everything that get tokenized and then eventually put
    into "nodes" on the Abstract Syntax Tree. SHOULD NEVER BE USED, WILL
    THROW ERRORS. Sole purpose is for class extension and comparison.
    """

    """
    List of dependencies. The encoder will look at all of these dependencies and
    add import statements for the language accordingly.
    """
    dependencies = {Python: set(), Java: set()}

    def indent(self, level: int) -> str:
        return "\t" * level

    def __str__(self) -> str:
        return self.__python__(0)

    def __python__(self, indent: int) -> str:
        raise NotImplementedError()

    def __java__(self, indent: int) -> str:
        raise NotImplementedError()

    def gatherDependencies(self, language: AbstractLanguage) -> set[Module]:

        """
        This basically loops through all of the instance variables of itself,
        checks each one to see if it is a ASTNode, and if it is, gathers its dependencies
        """

        deps = self.dependencies[language]
        for var_name, var_value in vars(self).items():
            if issubclass(var_value.__class__, ASTNode):
                deps = set.union(deps, var_value.gatherDependencies(language))

        return set(deps)

class ListOfASTNodes(ASTNode):
    """
    Acts as a list wrapper for ASTNodes. Will throw errors if constructor
    inputs are not all of type ASTNode. Includes various methods like
    gatherDependencies, iteration and item retrieval, as well as basic
    language methods.
    """

    def __init__(self, body: list[ASTNode]) -> None:

        # Shallow copy
        if isinstance(body, ListOfASTNodes):
            self.body = body.body
            return

        if not isinstance(body, list):
            body = [body]

        for token in body:
            if not issubclass(token.__class__, ASTNode):
                raise TypeError(f"ASTNodes in ListOfASTNodes must be of type ASTNode. Error thrown on {token}")

        self.body = body

    def gatherDependencies(self, language: AbstractLanguage) -> set[Module]:
        deps = set()
        for item in self.body:
            if issubclass(item.__class__, ASTNode):
                deps = set.union(deps, item.gatherDependencies(language))

        return deps

    def __str__(self) -> str:
        return self.__python__(0)

    def __python__(self, indent: int, char: str = "\n") -> str:
        return char.join([token.__python__(indent) for token in self.body])

    def __java__(self, indent: int,  char: str = "\n", semicolon: bool = True) -> str:
        return char.join([token.__java__(indent)
                + (";" if semicolon and token.__class__.__name__ != "Comment" else "") for token in self.body])

    def __iter__(self):
        return self.body.__iter__()

    def __getitem__(self, i: int):
        return self.body[i]

    def __len__(self):
        return len(self.body)

    def append(self, item: Union[ASTNode, "ListOfASTNodes"], to_front=False):
        """
        Can add one (or multiple items) to a list. Can add two
        lists together and specifiy a `to_front` param.
        """

        if isinstance(item, ListOfASTNodes):
            for i in item.body[::-1]:
                self.append(i, to_front=to_front)
        else:
            if to_front:
                self.body.insert(0, item)
            else:
                self.body.append(item)
        return self

class UnknownASTNode(ASTNode):
    """
    Used whenever the parser doesn't know what it's seeing (ie:
    variable names, stuff like that which aren't explicitly parsed
    out).
    """

    def __init__(self, content: str) -> None:
        self.content = content

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + self.content

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + self.content

    def __add__(self, other: str) -> str:
        return self.content + other

    def __radd__(self, other: str) -> str:
        return other + self.content

class Visibility:

    def visible(self, language: AbstractLanguage) -> str:
        match language:
            case Java:
                return "public " if self.public else "private "

        return ""

class CanBeStatic:

    def isStatic(self):
        return "static " if self.static else ""