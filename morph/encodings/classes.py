from morph.encodings.functions.base import CustomFunction
from morph.encodings.primitives import Declaration
from morph.languages.languages import Java
from typing import List
from morph.encodings.ASTNode import ListOfASTNodes, ASTNode, Visibility


class CustomClass(ASTNode, Visibility):
    """
    ASTNode representation of a user-defined class. Contains
    a name, class extension list, and a body.
    TODO: public/private/static.
    """

    def __init__(self, name: str, extensions: ListOfASTNodes, body: ListOfASTNodes, public: bool = True) -> None:

        self.name = name
        self.extensions = ListOfASTNodes(extensions)
        self.public = public

        self.static_declarations = ListOfASTNodes([])
        self.instance_declarations = ListOfASTNodes([])
        self.static_functions = ListOfASTNodes([])
        self.instance_functions = ListOfASTNodes([])

        for node in body:
            if isinstance(node, Declaration):
                if node.static:
                    self.static_declarations.append(node)
                else:
                    self.instance_declarations.append(node)
            elif isinstance(node, CustomFunction):
                if node.static:
                    self.static_functions.append(node)
                else:
                    self.instance_functions.append(node)

    def __python__(self, indent: int) -> str:

        # Basic stuff
        s = (
            self.indent(indent)
            + f"class {self.name}({self.extensions.__python__(0, ',')}):\n"
            + self.static_declarations.__python__(indent + 1, "\n")
            + "\n"
            + ("\n" if len(self.static_declarations) > 0 else "")
        )

        # Functions
        s += self.static_functions.__python__(indent + 1)
        s += self.instance_functions.__python__(indent + 1)

        # Instance variables
        if len(self.instance_declarations) > 0:
            init_params = ["self"] + [str(decl.name) for decl in self.instance_declarations]
            s += self.indent(indent + 1) + f"def __init__({', '.join(init_params)}):\n"
            for decl in self.instance_declarations:
                s += decl.__python__(indent + 2) + "\n"
            s += "\n"

        return s

    def __java__(self, indent: int) -> str:

        return (
            self.indent(indent)
            + self.visible(Java)
            + f"class {self.name}({self.extensions.__java__(0, ',')}){{\n\n"
            + self.static_declarations.__java__(indent + 1)
            + ("\n" if len(self.static_declarations) > 0 else "")
            + self.instance_declarations.__java__(indent + 1)
            + ("\n" if len(self.instance_declarations) > 0 else "")
            + "\n"
            + self.static_functions.__java__(indent + 1)
            + "\n"
            + self.instance_functions.__java__(indent + 1)
            + "\n"
            + self.indent(indent)
            + "}"
        )
