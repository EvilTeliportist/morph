from typing import List
from morph.encodings.ASTNode import ASTNode, ListOfASTNodes


# -------- TRY CATCH ------------
class Try(ASTNode):
    """
    ASTNode for a try/catch statement. This node will 'eat' the other nodes 
    related to it, like 'catch' and 'finally'. In the tree, this will be shown
    as the only node for the entire try/catch statement, so the other nodes like
    (catch/finally) will not be exposed.
    """

    def __init__(self, body: ListOfASTNodes, catches: ListOfASTNodes = ListOfASTNodes([])) -> None:

        self.body = body
        self.catches = catches

    def __python__(self, indent: int) -> str:

        return (
            self.indent(indent) + f"try:\n"
            + self.body.__python__(indent + 1)
            + "\n"
            + self.catches.__python__(indent)
            + '\n'
        )

    def __java__(self, indent: int) -> str:

        return (
            self.indent(indent) + f"try {{\n"
            + self.body.__java__(indent + 1)
            + "\n}" + ("\n" if len(self.catches) == 0 else " ") + self.catches.__java__(indent, " ")
        )


class Catch(ASTNode):
    def __init__(self, exception: str, body: ListOfASTNodes) -> None:

        self.body = body
        self.exception = exception

    def __python__(self, indent: int) -> str:

        return (
            f"except {self.exception.__python__(0)}:\n"
            + self.body.__python__(indent + 1)
        )

    def __java__(self, indent: int) -> str:

        return (
            f"catch {self.exception.__python__(0)} {{\n"
            + self.body.__python__(indent + 1)
            + "\n}"
        )


class Finally(ASTNode):
    def __init__(self, body: ListOfASTNodes):
        self.body = body

    def __python__(self, indent: int):
        return (
            self.indent(indent) + f"finally:\n"
            + self.body.__python__(indent + 1)
        )

    def __java__(self, indent: int):
        return (
            self.indent(indent) + f"finally {{\n"
            + self.body.__java__(indent + 1)
            + "\n}"
        )


# -------- EXCEPTIONS ------------
class Exception(ASTNode):

    pass
