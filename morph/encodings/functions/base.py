from typing import List
from morph.languages.languages import *
from morph.utils.exceptions import LanguageBlockedException
from morph.encodings.primitives import *
from morph.encodings.ASTNode import *


class AbstractFunction(ASTNode):
    """
    An abstract class for standard library functions.
    """

    """
    The morph tag for this function. This lets the decoder know how to interpret statements
    that could be function. When the decoder sees a tag, it'll generate an object of this class.
    """
    tag = None

    """
    Some features might be specific to certain languages. If the action object blocks
    any supported langauges, an error will be thrown.

    - Future note: this would be helpful to see in an IDE as well. For example, a
    VSCode extension could let you know which languages you can compile into due
    to your choice of action objects.
    """
    languages = []

    def __init__(self, params: ListOfASTNodes) -> None:

        self.tag = ""
        self.params = params

    def __python__(self, indent: int) -> str:
        if Python not in self.languages:
            raise LanguageBlockedException("Python", self)

    def __java__(self, indent: int) -> str:
        if Java not in self.languages:
            raise LanguageBlockedException("Java", self)


class CustomFunction(ASTNode, Visibility, CanBeStatic):
    """
    A user defined function. Takes a function name, function params, and
    a function body.
    """

    def __init__(
        self,
        returns: str,
        name: str,
        params: ListOfASTNodes,
        body: ListOfASTNodes,
        public: bool = True,
        static: bool = False,
    ) -> None:

        self.returns = returns
        self.name = name
        self.params = ListOfASTNodes(params)
        self.body = ListOfASTNodes(body)
        self.public = public
        self.static = static

    def __python__(self, indent: int) -> str:

        return (
            (self.indent(indent) + "@staticmethod\n" if self.static else "")
            + self.indent(indent)
            + f"def {self.name}({self.params.__python__(0, char=', ')}):\n"
            + self.body.__python__(indent + 1)
            + "\n"
        )

    def __java__(self, indent: int) -> str:

        return (
            self.indent(indent)
            + self.visible(Java)
            + self.isStatic()
            + f"{self.returns.__java__(0)} {self.name}({self.params.__java__(0, char=', ', semicolon=False)}){{\n"
            + self.body.__java__(indent + 1, semicolon=True)
            + "\n"
            + self.indent(indent)
            + f"}}"
        )


class FunctionParameter(ASTNode):
    def __init__(self, tag: PrimitiveTag, name: str) -> None:
        self.tag = tag
        self.name = name

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + self.name

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.tag.__java__(0)} {self.name}"


class FunctionCall(ASTNode):
    def __init__(self, name: str, params: ListOfASTNodes) -> None:
        self.name = name
        self.params = ListOfASTNodes(params)

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.name}({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.name}({self.params.__java__(0, semicolon=False, char=', ')})"


class Return(ASTNode):
    def __init__(self, body: ListOfASTNodes):
        self.body = body

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"return ({self.body.__python__(0)})"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"return ({self.body.__java__(0)})"


class Print(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "print"
    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"print({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"System.out.println({self.params.__java__(0)})"
