from morph.languages.languages import *
from morph.encodings.functions.base import AbstractFunction
from morph.encodings.dependencies.python.modules import py_mod_Math


class ASN_max(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "max"

    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"max({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.max({self.params.__java__(0)})"


class ASN_min(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "min"

    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"min({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.min({self.params.__java__(0)})"


class ASN_pow(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "pow"

    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"pow({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.pow({self.params.__java__(0)})"


class ASN_abs(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "abs"

    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"abs({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.abs({self.params.__java__(0)})"


class ASN_floor(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "floor"

    languages = [Python, Java]

    dependencies = {Python: set([py_mod_Math]), Java: set()}

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"math.floor({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.floor({self.params.__java__(0)})"


class ASN_ceil(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "ceil"

    languages = [Python, Java]

    dependencies = {Python: set([py_mod_Math]), Java: set()}

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"math.ceil({self.params.__python__(0)})"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"Math.ceil({self.params.__java__(0)})"


class ASN_mod(AbstractFunction):
    """
    Morph standard library function.
    """

    tag = "mod"

    languages = [Python, Java]

    def __python__(self, indent: int) -> str:
        super().__python__(0)
        return self.indent(indent) + f"{self.params[0].__python__(0)} % {self.params[1].__python__(0)}"

    def __java__(self, indent: int) -> str:
        super().__java__(0)
        return self.indent(indent) + f"{self.params[0].__java__(0)} % {self.params[1].__java__(0    )}"
