from morph.encodings.dependencies.base import PythonModule


class py_mod_Math(PythonModule):
    @staticmethod
    def __import__() -> str:
        return "import math"
