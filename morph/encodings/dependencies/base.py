class Module:
    """
    A dependencies module. Since almost every programming language that
    supports modular code and stuff like that uses modules, we can use
    modules here. Each module can have other submodules, which helps in
    recursive dependency detection. At the end of the encoding process,
    the encoder gathers all of the dependencies from the abstract syntax
    tree and lists them at the top of the file.
    """

    @staticmethod
    def __import__() -> str:
        raise NotImplementedError()


class PythonModule(Module):
    """
    A Python module, extension of the `Module` class.
    """
    pass


class JavaModule(Module):
    """
    A Java module, extension of the `Module` class.
    """
    pass
