from morph.languages.languages import Java
from morph.encodings.ASTNode import ASTNode, CanBeStatic, Visibility


class Comment(ASTNode):
    """
    AST node class for comments.
    """

    value = None

    def __init__(self, value: str) -> None:
        self.value = value[2:]  # Remove //'s

    def __python__(self, indent) -> str:
        return f'""" {self.value} """'

    def __java__(self, indent) -> str:
        return f"/* {self.value} */"


# PRIMITIVE VALUES
class Primitive(ASTNode):
    """
    AST node class for primitive variable types.
    Subclasses include integers, strings, booleans,
    etc.
    """

    value = None

    def __python__(self, indent) -> str:
        raise NotImplementedError()

    def __java__(self, indent) -> str:
        raise NotImplementedError()


class Integer(Primitive):
    def __init__(self, value: int) -> None:
        super().__init__()

        try:
            self.value = int(value)
        except:
            raise ValueError(f"Cannot cast '{value}' to type '{self.__class__.__name__}'.")

    def __python__(self, indent) -> str:
        return self.indent(indent) + f"{self.value}"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"{self.value}"


class String(Primitive):
    def __init__(self, value: str) -> None:
        super().__init__()

        try:
            self.value = str(value)  # Remove "'s
        except:
            raise ValueError(f"Cannot cast '{value}' to type '{self.__class__.__name__}'.")

    def __python__(self, indent) -> str:
        return self.indent(indent) + f'"{self.value}"'

    def __java__(self, indent) -> str:
        return self.indent(indent) + f'"{self.value}"'


class Boolean(Primitive):
    def __init__(self, value: bool) -> None:

        self.value = value

    def __python__(self, indent) -> str:
        return self.indent(indent) + str(self.value).capitalize()

    def __java__(self, indent) -> str:
        return self.indent(indent) + str(self.value).lower()

class Float(Primitive):
    def __init__(self, value: int) -> None:
        super().__init__()

        try:
            self.value = float(value)
        except:
            raise ValueError(f"Cannot cast '{value}' to type '{self.__class__.__name__}'.")

    def __python__(self, indent) -> str:
        return self.indent(indent) + f"{self.value}"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"{self.value}f"



# PRIMITIVE DECLARATIONS
class VariableTag(ASTNode):
    pass


class PrimitiveTag(VariableTag):
    """
    AST node class for variable types. For example, the
    `IntegerTag` subclass is for `int` tokens, and can
    be used to signal function params or variable declarations.
    """

    tag = None

    def __python__(self, indent) -> str:
        return ""


class IntegerTag(PrimitiveTag):

    tag = "int"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"int"


class FloatTag(PrimitiveTag):

    tag = "int"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"int"


class StringTag(PrimitiveTag):

    tag = "string"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"String"


class BooleanTag(PrimitiveTag):

    tag = "bool"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"boolean"


class CharTag(PrimitiveTag):

    tag = "char"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"char"


# VARIABLE DECLARATION
class Assignment(ASTNode):
    def __init__(self, left: ASTNode, right: ASTNode) -> None:

        self.left = left
        self.right = right

    def __python__(self, indent) -> str:
        return self.indent(indent) + f"{self.left.__python__(0)} = {self.right.__python__(0)}"

    def __java__(self, indent) -> str:
        return self.indent(indent) + f"{self.left.__java__(0)} = {self.right.__java__(0)}"


class Declaration(ASTNode, Visibility, CanBeStatic):

    def __init__(self, tag: VariableTag, name: str, value: ASTNode | str, public: bool = True, static: bool = False):
        self.tag = tag
        self.name = name
        self.value = value
        self.public = public
        self.static = static

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"{self.name} = {self.value.__python__(0)}"

    def __java__(self, indent: int) -> str:
        return (
            self.indent(indent) + self.visible(Java) + self.isStatic()
            + f"{self.tag.__java__(0)} {self.name} = {self.value.__java__(0)}"
        )