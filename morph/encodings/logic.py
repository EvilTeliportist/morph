from typing import List
from morph.encodings.ASTNode import ListOfASTNodes, ASTNode

# BOOLEAN LOGIC
class Comparator(ASTNode):
    """
    AST node class for logical comparisions (=<, >, ==).
    """

    symbol = "COMPARATOR"

    def __init__(self, left: ASTNode, right: ASTNode) -> None:

        self.right = right
        self.left = left

    def __python__(self, indent) -> str:

        return self.indent(indent) + f"({self.left.__python__(0)} {self.symbol} {self.right.__python__(0)})"

    def __java__(self, indent) -> str:

        return self.indent(indent) + f"({self.left.__java__(0)} {self.symbol} {self.right.__java__(0)})"


class DoubleEquals(Comparator):
    symbol = "=="


class LEQ(Comparator):
    symbol = "<="


class GEQ(Comparator):
    symbol = ">="


class GreaterThan(Comparator):
    symbol = ">"


class LessThan(Comparator):
    symbol = "<"


class NotEqual(Comparator):
    symbol = "!="


class LogicalOperator(ASTNode):
    """
    AST node class for logical operations.
    """
    pass

class Not(LogicalOperator):

    def __init__(self, body: ASTNode) -> None:
        self.body = body

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"not ({self.body.__python__(0)})"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"!({self.body.__java__(0)})"

class And(LogicalOperator):

    def __init__(self, left: ASTNode, right: ASTNode) -> None:
        self.right = right
        self.left = left

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"({self.left.__python__(0)}) and ({self.right.__python__(0)})"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"({self.left.__java__(0)}) && ({self.right.__java__(0)})"

class Or(LogicalOperator):

    def __init__(self, left: ASTNode, right: ASTNode) -> None:
        self.right = right
        self.left = left

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + f"({self.left.__python__(0)}) or ({self.right.__python__(0)})"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + f"({self.left.__java__(0)}) || ({self.right.__java__(0)})"


# ROUTING LOGIC
class Conditional(ASTNode):
    """
    AST node class for conditional operations, like `if`, `elif`, and `else`
    statements.
    """
    pass

class If(Conditional):
    """
    AST node class for `if` statements. `if` statements will each
    all of the other conditional statements that follow. For example,
    the token list [if, [conditional], [..stuff] else, [...stuff]] will
    be completely parsed into a single `if` node on the AST.
    """

    def __init__(
        self, condition: LogicalOperator, body: ASTNode, _else: ASTNode = None, elifs: ListOfASTNodes = None) -> None:

        self.condition = condition
        self.body = ListOfASTNodes(body)
        self._else = None if _else is None else ListOfASTNodes(_else)
        self.elifs = None if elifs is None else ListOfASTNodes(elifs)

    def __python__(self, indent: int) -> str:

        s = (
            self.indent(indent) + f"if ({self.condition.__python__(0)}):\n"
            + self.body.__python__(indent + 1) + "\n"
        )

        if self.elifs is not None:
            s += self.elifs.__python__(indent)
            s += '\n'

        if self._else is not None:
            s += self._else.__python__(indent)

        return s

    def __java__(self, indent: int) -> str:

        s = (
            self.indent(indent) + f"if ({self.condition.__java__(0)}){{\n"
            + self.body.__java__(indent + 1) + "\n"
            + self.indent(indent) + f"}}"
        )

        if self.elifs is not None:
            s += self.elifs.__java__(indent, semicolon=False, char="")

        if self._else is not None:
            s += self._else.__java__(0)

        return s

class ElseIf(Conditional):
    """
    AST node class for `elif` statements.
    """

    def __init__(self, condition: ListOfASTNodes, body: ListOfASTNodes) -> None:

        self.condition = condition
        self.body = ListOfASTNodes(body)

    def __python__(self, indent: int) -> str:

        return (
            self.indent(indent) + f"elif ({self.condition.__python__(0)}):\n"
            + self.body.__python__(indent + 1)
        )
        
    def __java__(self, indent: int) -> str:

        return (
            f" else if ({self.condition.__python__(0)}){{\n"
            + self.body.__java__(indent + 1, semicolon=True)
            + '\n' + self.indent(indent) + "}"
        )

class Else(Conditional):
    """
    AST node class for `else` statements.
    """

    def __init__(self, body: ListOfASTNodes) -> None:

        self.body = ListOfASTNodes(body)

    def __python__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"else:\n"
            + self.body.__python__(indent + 1)
        )

    def __java__(self, indent: int) -> str:
        return (
            f" else {{\n"
            + self.body.__java__(indent + 1, semicolon=True)
            + "\n" + self.indent(indent) + "}"
        )

class Switch(Conditional):
    
    def __init__(self, body: ASTNode, cases: ListOfASTNodes = None) -> None:
        self.body = body
        self.cases = [] if cases is None else ListOfASTNodes(cases)

    def __java__(self, indent: int) -> str:
        s =  self.indent(indent) + f"switch ({self.body.__java__(0)}){{"

        for case in self.cases:
            s += "\n"
            s += case.__java__(indent + 1)

        s += "\n" + self.indent(indent) + f"}}"
        return s

    def __python__(self, indent: int) -> str:
        s =  self.indent(indent) + f"match ({self.body.__python__(0)}):"

        for case in self.cases:
            s += "\n"
            s += case.__python__(indent + 1)

        s += "\n"
        return s

class Case(Conditional):
    def __init__(self, condition: ASTNode, body: ListOfASTNodes) -> None:
        self.body = ListOfASTNodes(body)
        self.condition = condition

    def __java__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"case {self.condition.__java__(0)}: "
            + self.body.__java__(0)
        )

    def __python__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"case {self.condition.__python__(0)}: "
            + self.body.__python__(0)
        )