# morph.encodings

This package holds all of the token objects used in the parsing process of the `morph.engines` package. We'll go through each file and subpackage individually:

- `morph.encodings.dependencies`
- `morph.encodings.functions`
- `logic.py`
- `primitives.py`
- `tags.py`
- `token.py`

## Dependencies

This module has a submodule for each supported language, but it's almost entirely for organization. The `base.py` file holds the important bit, forcing each class that inherets the `Module` class (of which `<Language>Module` inherets from) to support a static `__import__` function. Once the dependencies are gathered by the encoder (see more: `morph/engines/README.md`), `__import__` gets called on each one to generate the import statements required.

## Functions

The `functions` module contains all of the functions currently supported by Morph. Each `.py` file (other than `init.py` and `base.py`) contains a subset of functions that Morph can detect and encode correctly. These are known as Morph's standard library. Each function can have some children, mainly the parameters. Each function **has** to override the `__<language>__()` functions present in the `AbstractFunction` definition, much like AST nodes. Once a function is added and set to inheret from `AbstractFunction`, the parser will automatically search for it when parsing (again, see `morph/engine/README.md` for more information on the parsing process).

Each function also can define a `language` list. By default this is empty, and trying to encode a function in an unsupported language will raise a `LanguageBlockedException`. Just ensure that you add a class reference to each supported language (from `morph.languages.languages`) in the list so that the encoder knows it can encode the function into the language.

## Logic

All of the AST nodes in the `logic.py` file handle logical operators, conditional statements, etc. Everything that has to do with a languages logical flow within classical OOP languages.

## Primitives

Here we see all of the primitives that Morph currently supports (including comments). It also holds the token form of primitive tags (ex: int, bool, str, etc). Lastly, it holds the `Assignment` and `Declaration` classes, which can encode into the proper variable declaration and assignment format for a language.

## Tags

Holds the tags that can trigger the Morph decoder/encoder to work differenly. Currently only supports the `LanguageTag`.

## ASTNode

Contains the `ASTNode` class as well as multiple subclasses, such as `ListOfASTNodes`, `UnknownASTNode`, as well as interfaces for nodes to extend from like `CanBeStatic` and `Visibility`.
