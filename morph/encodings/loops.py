from morph.encodings.ASTNode import ListOfASTNodes, ASTNode

class Loop(ASTNode):
    """
    Abstract class for any kind of looping AST nodes.
    """

    def __init__(self, condition: ListOfASTNodes, body: ListOfASTNodes) -> None:

        self.condition = condition
        self.body = ListOfASTNodes(body)

class ForLoop(Loop):
    """
    AST node for `for` loops.
    """

    def __init__(self, condition: ListOfASTNodes, body: ListOfASTNodes) -> None:
        
        self.varname = condition[0].left.name
        self.start = condition[0].right
        self.end = condition[1].right
        self.increment = condition[2]
        self.body = body

    def __python__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"for {self.varname} in range({self.start.__python__(0)}, {self.end.__python__(0)}, {self.increment.right.__python__(0)}):\n"
            + self.body.__python__(indent + 1)
            + "\n"
        )

    def __java__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"for (int {self.varname} = {self.start.__java__(0)}; {self.varname} < {self.end.__java__(0)}; {self.varname} = {self.increment.__java__(0)}){{\n"
            + self.body.__java__(indent + 1, semicolon=True)
            + "\n" + self.indent(indent) + '}'
        )

class WhileLoop(Loop):
    """
    AST node for `while` loops.
    """

    def __python__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"while ({self.condition.__python__(0)}):\n"
            + self.body.__python__(indent + 1)
            + "\n"
        )

    def __java__(self, indent: int) -> str:
        return (
            self.indent(indent) + f"while ({self.condition.__java__(0)}){{\n"
            + self.body.__java__(indent + 1, semicolon=True)
            + "\n" + self.indent(indent) + '}'
        )

class Continue(ASTNode):
    """
    AST node for `continue` statements.
    """

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + "continue"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + "continue"

class Break(ASTNode):
    """
    AST node for `break` statements.
    """

    def __python__(self, indent: int) -> str:
        return self.indent(indent) + "break"

    def __java__(self, indent: int) -> str:
        return self.indent(indent) + "break"