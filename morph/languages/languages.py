
class AbstractLanguage:

    fileExtension = ""
    arrayBrackets = []
    semicolonLineEnders = True
    name = ""

    def __init__(self):
        pass

    def metadata(self):
        """
        Lets an encoder get file metadata associated with the language.
        """
        pass

    @staticmethod
    def getVariableType(self, varType: str) -> str:
        """
        Maps the .morph handled variable types to the type
        names of the language
        """
        pass


class Python(AbstractLanguage):

    fileExtension = ".py"
    arrayBrackets = ["[", "]"]
    semicolonLineEnders = False
    name = "Python"

    def metadata(self):
        return super().metadata()

    def __str__(self):
        return "Python"

    @staticmethod
    def getVariableType(self, varType: str) -> str:
        return ""


class Java(AbstractLanguage):

    fileExtension = ".java"
    arrayBrackets = ["{", "}"]
    semicolonLineEnders = True
    name = "Java"

    def metadata(self):
        return super().metadata()

    def __str__(self):
        return "Java"

    @staticmethod
    def getVariableType(self, varType: str) -> str:
        match varType:
            case "int": return 'int'
            case "string": return 'String'
            case 'float': return 'float'
            case 'bool': return 'boolean'
            case _: raise KeyError(f"Variable type {varType} not supported.")
