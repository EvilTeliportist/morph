from morph.encodings.ASTNode import ASTNode, ListOfASTNodes
from morph.config.config import ALLOWED_VARIABLE_NAMES
import re

def CheckExists(words, current_index, offset) -> None:

    if len(words) - 1 < (current_index + offset):
        raise SyntaxError(f"Expected something {offset} term(s) after '{words[current_index]}'")

def CheckForArray(words, current_index, offset):

    if not isinstance(words[current_index + offset], ListOfASTNodes):
        raise SyntaxError(
            f"Expected array {offset} terms after {words[current_index]}. Got {words[current_index+offset]} instead."
        )

def CheckWordType(words, current_index, offset, type):
    if not issubclass(words[current_index + offset].__class__, type):
        raise SyntaxError(f"Token '{words[current_index + offset]}' of type '{words[current_index + offset].__class__}' is not of type '{type}'")

def CheckNotASTNode(words, current_index, offset):
    if isinstance(words[current_index + offset], ASTNode):
        raise SyntaxError(
            f"Expected non-token term {offset} terms after {words[current_index]}. Got {words[current_index+offset].__class__} instead."
        )

def CheckNotArray(words, current_index, offset):

    if isinstance(words[current_index + offset], ListOfASTNodes):
        raise SyntaxError(
            f"Expected non-array {offset} terms after {words[current_index]}. Got {words[current_index+offset].__class__} instead."
        )

def CheckArrayLength(words, current_index, offset, length):
    
    CheckForArray(words, current_index, offset)

    if len(words[current_index + offset]) != length:
        raise IndexError(f"There must be exactly {length} tokens after the tag '{words[current_index]}'.")

def CheckArrayWordType(words, current_index, offset, word_position, type):

    CheckForArray(words, current_index,  offset)

    CheckWordType(words[current_index + offset], 0, word_position, type)

def CheckValidVariableName(words, current_index, offset):
    if not re.match(re.compile("^" + ALLOWED_VARIABLE_NAMES + "$"), words[current_index + offset]):
        s = f"""'{words[current_index + offset]}' is not an allowed variable name.
        Variable names must start with a letter or underscore, and can only
        contain alphanumerics and underscores after the first character.
        """
        raise SyntaxError(s)