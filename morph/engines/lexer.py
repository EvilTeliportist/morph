reserved = {
    # Conditonal Logic
    "if": "IF",
    "else": "ELSE",
    "elif": "ELIF",
    "switch": "SWITCH",
    "case": "CASE",
    # Primitives
    "int": "INT_TAG",
    "float": "FLOAT_TAG",
    "bool": "BOOL_TAG",
    "string": "STR_TAG",
    "char": "CHAR_TAG",
    # Functions and Classes
    "function": "FUNC",
    "class": "CLASS",
    "public": "PUBLIC",
    "private": "PRIVATE",
    "static": "STATIC",
    "return": "RETURN",
    # Loops
    "for": "FOR",
    "while": "WHILE",
    "break": "BREAK",
    "continue": "CONTINUE",
}

tokens = [
    # OPERATORS #
    "PLUS",  # +
    "MINUS",  # -
    "MULTIPLY",  # *
    "DIVIDE",  # /
    "MODULO",  # %
    "NOT",  # !
    "EQUALS",  # =
    # COMPARATORS #
    "LT",  # <
    "GT",  # >
    "LEQ",  # <=
    "GEQ",  # >=
    "DOUBLEEQUAL",  # ==
    "NEQ",  # !=
    "AND",  # &
    "OR",  # |
    # BRACKETS #
    "LPAREN",  # (
    "RPAREN",  # )
    "LBRACE",  # [
    "RBRACE",  # ]
    "BLOCKSTART",  # {
    "BLOCKEND",  # }
    "SEMI",  # ;
    # DATA TYPES#
    "INTEGER",  # int
    "FLOAT",  # dbl
    "BOOL",  # bool
    "STRING",  # string
    "CHAR",  # char
    "COMMENT",  # --
    "COMMA",
    "PERIOD",
    "COLON",
    "ID",  # any identifier
] + list(reserved.values())


# Regular expression rules for simple tokens
t_PLUS = r"\+"
t_MINUS = r"-"
t_MULTIPLY = r"\*"
t_DIVIDE = r"/"
t_MODULO = r"%"
t_LPAREN = r"\("
t_RPAREN = r"\)"
t_LBRACE = r"\["
t_RBRACE = r"\]"
t_BLOCKSTART = r"\{"
t_BLOCKEND = r"\}"
t_SEMI = r"\;"
t_NOT = r"\!"
t_EQUALS = r"\="
t_GT = r"\>"
t_LT = r"\<"
t_LEQ = r"\<\="
t_GEQ = r"\>\="
t_DOUBLEEQUAL = r"\=\="
t_NEQ = r"\!\="
t_AND = r"\&"
t_OR = r"\|"
t_COMMENT = r"\/\/.*"
t_COMMA = r"\,"
t_PERIOD = r"\."
t_COLON = r"\:"
t_ignore = " \t"  # ignore spaces and tabs

# Rules for primitive tokens
def t_INTEGER(t):
    r"\d+"
    t.value = int(t.value)
    return t


def t_FLOAT(t):
    r"(\d*\.\d+)|(\d+\.\d*)"
    t.value = float(t.value)
    return t


def t_STRING(t):
    r"(\".*\")"
    t.value = t.value[1:-1]
    return t


def t_CHAR(t):
    r"(\'.\')"
    t.value = t.value[1:-1]
    return t


def t_BOOL(t):
    r"(true|false)"
    t.value = t.value == "true"
    return t


# Define a rule so we can track line numbers
def t_newline(t):
    r"\n+"
    t.lexer.lineno += len(t.value)


# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


def t_ID(t):
    r"[a-zA-Z_][a-zA-Z_0-9]*"
    t.type = reserved.get(t.value, "ID")  # Check for reserved words
    return t
