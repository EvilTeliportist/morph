from typing import List

from morph.languages.languages import *
from morph.encodings.ASTNode import ASTNode, ListOfASTNodes
from morph.config.nonterminals import NONTERMINAL_NODES


class AbstractEncoder:
    """
    Abstract class for encoders. Currently only one
    encoder is implemented (without plans for others)
    but in case of others in the future, this is the
    basic template for them.
    """

    def __init__(self, language: AbstractLanguage, stack: List[ASTNode], verbose: bool = False) -> None:
        raise NotImplementedError()

    def encode(self, stack):
        raise NotImplementedError()

    def log(self, message, level):
        if level <= self.verbose:
            print(message)

    def clear(self):
        raise NotImplementedError()

class BasicEncoder(AbstractEncoder):
    """
    A basic encoder class that takes input directly from
    the `parse` method of decoders. The encoder generates
    an `output` with the `encode` method. This output is 
    just a list of strings, but can be passed into the
    `write` method to be written to file.

    NOTE: After use, the encoder must be cleared with the
    `clear` method before being used again.
    """

    def __init__(self, verbose: int) -> None:
        self.verbose = verbose
        self.dependencies = set([])
        self.output = []

    def encode(self, stack: ListOfASTNodes, language: AbstractLanguage) -> list[str]:

        self.log("==== BEGINNING THE ENCODING PROCESS =====", 2)

        # ------- Gather all dependencies -----------
        self.log("\n------ Gathering Dependencies From ---------", 3)
        for token in stack:
            self.log(token.__class__, 3)
            self.dependencies = set.union(self.dependencies, token.gatherDependencies(language))
 

        self.log("\n------ Final Dependencies ---------", 3)
        for item in self.dependencies:
            self.log(item, 3)
            self.output.append(item.__import__() + '\n')

        if len(self.output) > 0:
            self.output.append('\n')

        # ------------  Export AST --------------
        match language.name:
            case "Python":
                self.output.append(stack.__python__(0))
            case "Java":
                self.output.append(stack.__java__(0, semicolon=True))

        self.log("\n------ Abstract Syntax Tree ---------", 3)
        [self.log(item, 3) for item in stack]
        self.log("\n", 3)

        self.log("\n------ Final Encoded Output ---------", 3)
        [self.log(item, 3) for item in self.output]
        self.log("\n", 3)

        self.log("==== FINISHED THE ENCODING PROCESS =====", 2)

        return self.output

    def write(self, orig_path: str, output: list[str], language: AbstractLanguage):

        file_path = orig_path.replace(".morph", language.fileExtension)

        with open(file_path, 'w') as file:
            file.truncate()
            file.write("".join(output))

    def clear(self):
        self.output = []
        self.dependencies = set([])