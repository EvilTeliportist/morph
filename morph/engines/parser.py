from re import L
import ply.yacc as yacc
import ply.lex as pylex
import logging

logging.basicConfig(
     level = logging.DEBUG,
     filename = "parselog.txt",
     filemode = "w",
     format = "%(filename)10s:%(lineno)4d:%(message)s"
)
log = logging.getLogger()


from morph.engines.lexer import *
from morph.encodings.ASTNode import *
from morph.encodings.loops import *
from morph.encodings.primitives import *
from morph.encodings.tags import *
from morph.encodings.logic import *
from morph.encodings.classes import *
from morph.encodings.operators import *
from morph.encodings.exceptions import *
from morph.encodings.functions.base import *
from morph.encodings.functions.math import *

from morph.engines.patterns import *


precedence = ( # Lowest to highest precedence for shifting/reducing
    ('nonassoc', 'LT', 'GT', 'LEQ', 'GEQ'),
    ('left', 'SEMI'),
    ('left', 'ID'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULTIPLY', 'DIVIDE'),
    ('right', 'NEGATIVE'),
    ('left', 'EQUALS'),
    ('left', 'LPAREN', 'RPAREN'),
)


# Base
def p_exp(p):
    """
    exp : exp SEMI
        | primitive
        | log_op
        | if_stmt
        | decl 
        | assignment
        | function
        | id
        | class
        | func_call
        | switch
        | return
        | break
        | continue
        | wloop
    """
    p[0] = p[1]

def p_paren(p):
    'exp : LPAREN exp RPAREN'
    p[0] = p[2]

def p_multiple_exp(p):
    """
    exp : exp SEMI exp
        | exp COMMENT
        | COMMENT exp
    """

    if len(p) == 3: # if comment

        if isinstance(p[1], ListOfASTNodes):
            p[0] = p[1].append(Comment(p[2]))
        elif isinstance(p[2], ListOfASTNodes):
            l = []
            l.append(Comment([p[1]]))
            l = l + p[2].body
            p[0] = ListOfASTNodes(l)
        else:
            if p[1].startswith("//"):
                p[0] = ListOfASTNodes([Comment(p[1]), p[2]])
            else:
                p[0] = ListOfASTNodes([p[1], Comment(p[2])])

    else:
        l = []

        if isinstance(p[1], ListOfASTNodes):
            l += p[1].body
        else:
            l.append(p[1])

        if isinstance(p[3], ListOfASTNodes):
            l += p[3].body
        else:
            l.append(p[3])

        p[0] = ListOfASTNodes(l)

def p_id(p):
    """
    id : ID
    """
    p[0] = UnknownASTNode(p[1])


# Modifiers (public/private/static)'
def p_visibility(p):
    """
    visibility : PUBLIC
               | PRIVATE
    """
    p[0] = p[1]


# Primitive types
def p_int(p):
    'int : INTEGER'
    p[0] = Integer(p[1])

def p_float(p):
    'float : FLOAT'
    p[0] = Float(p[1])

def p_bool(p):
    'bool : BOOL'
    p[0] = Boolean(p[1])

def p_str(p):
    'str : STRING'
    p[0] = String(p[1])

def p_char(p):
    'char : CHAR'
    p[0] = String(p[1])

def p_primitive(p):
    """ 
    primitive : int
              | float
              | bool
              | str
              | char
    """
    p[0] = p[1]


# Declarations and Assignments
def p_var_tag(p):
    """
    var_tag : INT_TAG
            | FLOAT_TAG
            | BOOL_TAG
            | STR_TAG
            | CHAR_TAG
            | id
    """
    match p[1]:
        case "int":
            p[0] = IntegerTag()
        case "float":
            p[0] = FloatTag()
        case "bool":
            p[0] = BooleanTag()
        case "string":
            p[0] = StringTag()
        case "char":
            p[0] = CharTag()
        case _:
            p[0] = p[1]

def p_declaration(p):
    """
    decl : var_tag id EQUALS exp
         | visibility var_tag id EQUALS exp
         | visibility STATIC var_tag id EQUALS exp
    """
    if len(p) == 5:
        p[0] = Declaration(p[1], p[2], p[4])
    elif len(p) == 6:
        p[0] = Declaration(p[2], p[3], p[5], public=(p[1] == "public"))
    else:
        p[0] = Declaration(p[3], p[4], p[6], public=(p[1] == "public"), static=True)

def p_assignment(p):
    """
    assignment : id EQUALS exp
    """
    p[0] = Assignment(p[1], p[3])


# Operators
def p_add(p):
    """
    exp : exp PLUS exp
    """
    p[0] = Add(p[1], p[3])

def p_subtract(p):
    """
    exp : exp MINUS exp
    """
    p[0] = Subtract(p[1], p[3])

def p_mult(p):
    """
    exp : exp MULTIPLY exp
    """
    p[0] = Multiply(p[1], p[3])

def p_divide(p):
    """
    exp : exp DIVIDE exp
    """
    p[0] = Divide(p[1], p[3])

def p_mod(p):
    """
    exp : exp MODULO exp
    """
    p[0] = Modulo(p[1], p[3])
    
def p_neg(p):
    """
    exp : MINUS exp %prec NEGATIVE
    """
    p[0] = Negate(p[2])


# Logical Operators
def p_gt(p):
    """
    log_op : exp GT exp
    """
    p[0] = GreaterThan(p[1], p[3])

def p_lt(p):
    """
    log_op : exp LT exp
    """
    p[0] = LessThan(p[1], p[3])

def p_gte(p):
    """
    log_op : exp GEQ exp
    """
    p[0] = GEQ(p[1], p[3])

def p_lte(p):
    """
    log_op : exp LEQ exp
    """
    p[0] = LEQ(p[1], p[3])

def p_eq(p):
    """
    log_op : exp DOUBLEEQUAL exp
    """
    p[0] = DoubleEquals(p[1], p[3])

def p_neq(p):
    """
    log_op : exp NEQ exp
    """
    p[0] = NotEqual(p[1], p[3])

def p_not(p):
    """
    log_op : NOT exp
    """
    p[0] = Not(p[2])

def p_and(p):
    """
    log_op : exp AND exp
    """
    p[0] = And(p[1], p[3])

def p_or(p):
    """
    log_op : exp OR exp
    """
    p[0] = Or(p[1], p[3])


# Functions
def p_param(p):
    """
    param : var_tag id
          | param COMMA param
    """

    if len(p) == 4:
        if isinstance(p[3], ListOfASTNodes):
            p[0] = p[3].append(p[1], to_front=True)
        else:
            p[0] = ListOfASTNodes([p[1], p[3]])
    else:
        p[0] = FunctionParameter(p[1], p[2])

def p_func(p):
    """
    function : visibility var_tag ID LPAREN RPAREN block
             | visibility var_tag ID LPAREN param RPAREN block
    """
    if len(p) == 7:
        p[0] = CustomFunction(p[2], p[3], [], p[6], public=(p[1] == "public"))
    else:
        p[0] = CustomFunction(p[2], p[3], p[5], p[7], public=(p[1] == "public"))

def p_static_func(p):
    """
    function : visibility STATIC var_tag ID LPAREN RPAREN block
             | visibility STATIC var_tag ID LPAREN param RPAREN block
    """
    if len(p) == 7:
        p[0] = CustomFunction(p[3], p[4], [], p[7], public=(p[1] == "public"), static=True)
    else:
        p[0] = CustomFunction(p[3], p[4], p[6], p[8], public=(p[1] == "public"), static=True)

def p_func_call(p):
    """
    func_call : id LPAREN exp RPAREN
    """
    for subclass in AbstractFunction.__subclasses__():
        if subclass.tag == str(p[1]):
            p[0] = subclass(p[3])
            return

    p[0] = FunctionCall(p[1], p[3])

def p_return(p):
    """
    return : RETURN exp
    """
    p[0] = Return(p[2])


# Classes
def p_class(p):
    """
    class : visibility CLASS ID LPAREN RPAREN block
    """
    p[0] = CustomClass(p[3], [], p[6], public=(p[1] == "public"))


# Code Blocks
def p_block(p):
    """
    block : BLOCKSTART exp BLOCKEND
    block : BLOCKSTART BLOCKEND
    """
    if len(p) == 4:
        p[0] = ListOfASTNodes(p[2])
    else:
        p[0] = ListOfASTNodes([])


# Conditional Logic
def p_if_else(p):
    """
    if_stmt : IF LPAREN exp RPAREN block
            | IF LPAREN exp RPAREN block else_stmt
            | IF LPAREN exp RPAREN block elif_stmt
            | IF LPAREN exp RPAREN block elif_stmt else_stmt
    """
    if len(p) == 6: # only if
        p[0] = If(p[3], p[5])
    elif len(p) == 7 and isinstance(p[6], Else): # must be if/else
        p[0] = If(p[3], p[5], _else=p[6])
    elif len(p) == 7: # must be if/elif
        p[0] = If(p[3], p[5], elifs=p[6])
    elif len(p) == 8:
        p[0] = If(p[3], p[5], elifs=p[6], _else=p[7])

def p_else(p):
    """
    else_stmt : ELSE block
    """
    p[0] = Else(p[2])

def p_elif(p):
    """
    elif_stmt : ELIF LPAREN exp RPAREN block
              | elif_stmt ELIF LPAREN exp RPAREN block
    """

    if len(p) == 6:
        p[0] = ElseIf(p[3], p[5])
    else:
        l = []
        if isinstance(p[1], ListOfASTNodes):
            l += p[1].body
        else:
            l.append(p[1])
        l.append(ElseIf(p[4], p[6]))
        p[0] = ListOfASTNodes(l)

def p_switch(p):
    """
    switch : SWITCH LPAREN exp RPAREN BLOCKSTART case BLOCKEND
    """
    p[0] = Switch(p[3], p[6])

def p_case(p):
    """
    case : CASE exp COLON exp
         | case CASE exp COLON exp
    """
    if len(p) == 5:
        p[0] = Case(p[2], p[4])
    else:
        l = []
        if isinstance(p[1], ListOfASTNodes):
            l += p[1].body
        else:
            l.append(p[1])
        l.append(Case(p[3], p[5]))
        p[0] = ListOfASTNodes(l)
        

# Loops
def p_break(p):
    """
    break : BREAK
    """
    p[0] = Break()

def p_continue(p):
    """
    continue : CONTINUE
    """
    p[0] = Continue()

def p_while_loop(p):
    """
    wloop : WHILE LPAREN exp RPAREN block
    """
    p[0] = WhileLoop(p[3], p[5])


# Error rule for syntax errors
def p_error(p):
    if p is None:
        print("Unexpected end of phrase.")
    else:
        print(f"Unexpected token: {p}")


# Build the parser
lexer = pylex.lex() #(errorlog=yacc.NullLogger())
parser = yacc.yacc() #(debug=False, errorlog=yacc.NullLogger())


def parse(s: str) -> ListOfASTNodes:
    x = ListOfASTNodes(parser.parse(s, debug=1))
    return x