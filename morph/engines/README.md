# morph.engines

This package holds all of the engines and layers required for decoding and encoding a `.morph` file.

## Examples

For all examples in this documentation, were going to be using a sample `.morph` file. Here it is:

```
LANGUAGE Java;

int x = 5;

if (x < 5){
    print("X is less than 5");
} else {
    print("X is not less than 5");
}
```

## Lexing

See [PLY Lex-Yacc](https://www.dabeaz.com/ply/ply.html) documentation for information about lexing.

## Parsing

See [PLY Lex-Yacc](https://www.dabeaz.com/ply/ply.html) documentation for information about parsing.

Here is the parsed output of our example `.morph` file.

```
[
<morph.encodings.primitives.VariableDeclaration object at 0x000001E476E42EF0>,
<morph.encodings.logic.If object at 0x000001E476E43040>
]
```

The reason that there are only two elements is because the `if` object holds all of the rest of the information inside its body and else statement. The tree looks like this:

```
<VariableDeclaration Object>
<If Object>
    | <Comparator object> // This is the (x < 5)
    | <Print object>
        | <String object>
            | "X is less than 5"
    | <Else object>
        | <Print object>
            | <String object>
                | "X is not less than 5"
```

# The Encoder

Compared to decoding, encoding is pretty simple. There are really only three parts:

- Generating raw output
- Gathering dependencies
- (TODO): File formatting and exporting

## Raw Output

Everything that inherets from the `ASTNode` class has to have a `__<language name>__(indent: int)` method for all languages that Morph supports. Each object knows how to combine its child objects to form a correctly formatted string for the language, so all we have to do is call `__java__(0)` on all of the root elements and the `__java__(0)` call will execute recursivly down the tree (incrementing the indent when necissary) until all objects return their string representation in Java. Now, we have our final Java output:

```
int x = 5;
if (x < 5){
    System.out.println("X is less than 5")
} else {
    System.out.println("X is not less than 5")
};
```

## Gathering Dependencies

Some functions have dependencies that need to be imported. An example of this is the `floor` function. In Java, you need the `Math` module to run `Math.floor`. However, this is part of the standard Java library, so we dont need to import it. Instead, the `__java__()` function of the `floor` class knows to put the `Math` tag at the start. For Python however, we have to use the `math` module. This module is not part of the standard library, so we need to include `import math` at the top of the file for the Python to be able to compile. To work around this, every function can define dependencies for each language. The encoder calls `gatherDependencies()` on each root element of the tree, and the same recursive process takes effect. Eventually, all of the required dependencies arrive at the top and are given to the encoder.
