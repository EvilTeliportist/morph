import re
from morph.encodings.ASTNode import ASTNode


class AbstractPattern:
    """
    A pattern is a class that allows you to specifiy a specific pattern, then
    run the pattern.match() method to see if there is a match. The match method
    takes a list of strings or ASTNodes as well an index, and it will tell you
    whether the pattern matches at that index.
    """

    def match(self, words: list[str | ASTNode], index: int = 0) -> bool:
        raise NotImplementedError()


class RegexPattern(AbstractPattern):
    """
    This is just a simple Regex pattern. Specify the regex expression on init,
    and calling match will compile and check the specific list index against
    the regex pattern.
    """

    def __init__(self, rexp: str) -> None:
        self.rexp = re.compile(rexp)

    def match(self, words: list[str | ASTNode], index: int = 0) -> bool:

        if not isinstance(words[index], str):
            return False

        return self.rexp.match(words[index])


class TypeSequencePattern(AbstractPattern):
    """
    This allows you to specify a pattern of types, ie: [PrimitiveTag, str, Primitive].
    It can be used to parse multi-word expressions into the AST.
    """

    def __init__(self, sequence: list[ASTNode | str]) -> None:
        self.sequence = sequence

    def match(self, words: list[str | ASTNode], index: int = 0) -> bool:

        if len(words) < index + len(self.sequence):
            return False

        for i in range(len(self.sequence)):
            if not issubclass(words[index + i].__class__, self.sequence[i]):
                return False

        return True


class MixedPattern(AbstractPattern):
    """
    This pattern offers mixed support of both the RegexPattern and the
    TypeSequencePattern. You can specify a sequence of types and regex
    expressions, and the pattern will check each type and expression.
    Ex: [PrimitiveTag, r"\\=", Primitive]
    """

    def __init__(self, sequence: list[ASTNode | str]) -> None:
        self.sequence = sequence

    def match(self, words: list[str | ASTNode], index: int = 0) -> bool:

        if len(words) < index + len(self.sequence):
            return False

        for i in range(len(self.sequence)):

            if isinstance(self.sequence[i], str):

                rexp = re.compile(self.sequence[i])
                if not rexp.match(words[index + i]):
                    return False
            else:

                if not issubclass(words[index + i].__class__, self.sequence[i]):
                    return False

        return True
