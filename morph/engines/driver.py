from morph.languages.languages import Java, Python
import sys
import getopt
from morph.engines.encoder import BasicEncoder
from morph.testing.tester import Tester
from morph.engines.parser import parse


if __name__ == "__main__":

    # Get Command Line args, discard first argument because it's just 'driver.py'
    path = ""
    verbose = 1

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hp:v:", ["path=", "verbose="])
    except getopt.GetoptError:
        print("morph/engines/driver.py -p <path> -v <verbose level>")
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print("morph/engines/driver.py -p <path> (-v) <verbose level>")
            sys.exit()
        elif opt in ("-p", "--path"):
            path = arg
        elif opt in ("-v", "--verbose"):
            verbose = int(arg)

    # Check if path is supplied
    if path == "":
        print("Please provide a valid path to a .morph file via the -p argument.")
        sys.exit()

    lang, file = Tester.openFile(path)
    stack = parse(file)
    encoder = BasicEncoder(verbose=verbose)
    output = encoder.encode(stack, lang)
    encoder.write(path, output, lang())
