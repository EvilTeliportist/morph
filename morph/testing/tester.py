from typing import Tuple
from morph.utils.tools import print_with_colors
from morph.languages.languages import AbstractLanguage
from morph.engines.encoder import BasicEncoder
from morph.encodings.dependencies.base import Module
from morph.engines.parser import parse
from morph.config.config import FILE_EXTENSION

import traceback, sys, re

class Tester():

    test_path = "morph/testing/"
    encoder = BasicEncoder(verbose=0)

    @staticmethod
    def openFile(path: str) -> Tuple[AbstractLanguage, str]:
        """
        Opens the file at the path specified. File must be a .morph file.
        Detects the language specified at the top and returns the language
        and the rest of the string text.
        """

        try:

            lang = None

            # Check extension
            if not path.endswith(FILE_EXTENSION):
                print(f"Please specify a file of type {FILE_EXTENSION}")
                sys.exit()

            # Open file
            file = open(path, "r")

            # Detect language
            language_tag = file.readline().replace("\n", "").split(" ")
            match language_tag:
                case ["LANGUAGE", s]:
                    for language in AbstractLanguage.__subclasses__():
                        if language.name == s[:-1]:
                            lang = language
                case _:
                    raise SyntaxError("Please include a proper language tag.")


            return lang, file.read()

        except OSError as e:
            print(f"Could not open file at: {path}")
            traceback.print_exc()
            sys.exit()

    @staticmethod
    def test(file_path: str,
            expected_dependencies: set[Module],
            language: AbstractLanguage):

        errors = []

        try:

            language, file = Tester.openFile(file_path)
            stack = parse(file)
            output = Tester.encoder.encode(stack, language)
            dependencies = Tester.encoder.dependencies
            Tester.encoder.write(file_path, output, language)

            # Check dependencies
            if dependencies != expected_dependencies:
                errors.append(f"[{file_path}: {language.name}]: Dependecies incorrect. Expected {expected_dependencies} but got {dependencies}")


            # Open output file
            with open(file_path.replace(".morph", language.fileExtension), 'r') as file:

                # Open correct output file
                correct_file_path = "/".join(file_path.split('/')[:-1]) + '/' + file_path.split('/')[-1].replace("test", "correct_test").replace(".morph", language.fileExtension)
                with open(correct_file_path) as correct_file:

                    # Get file contents
                    encoded_output = file.read()
                    correct_output = correct_file.read()

                    # Compare
                    if encoded_output.strip() != correct_output.strip():
                        errors.append(f"[{file_path}: {language.name}]: Output not equal.")


            Tester.encoder.clear()

        except Exception as e:
            errors.append(traceback.format_exc())

        if len(errors) == 0:
            print_with_colors(language.name + " ", "CGREEN", end='')
        else:
            
            print_with_colors("x" + (" " *  len(language.name)), "CRED", end='')
            
        return errors