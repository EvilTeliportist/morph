class AbstractException(Exception):
    pass

class SyntaxError(AbstractException):
    def __init__(self, line: int, word: int, message: str):
        self.message = f"Error at [Line: {line} / Word: {word}]: {message}"
        super().__init__(self.message)

class LanguageBlockedException(AbstractException):
    def __init__(self, language, caller):
        self.message = (
            f"Error while trying to compile the '{caller.__class__.__name__}' action object. "
            f"This action object blocks the language '{language}'. Either remove the code that "
            f"utilizes the action object or compile into a different, non-blocked language."
        )
        super().__init__(self.message)

class StringError(AbstractException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

class FunctionError(AbstractException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
