from morph.engines.patterns import *


def test_regex():
    s = r"abc*"
    pattern = RegexPattern(s)
    assert pattern.match(["ab"])
    assert pattern.match(["abc"])
    assert pattern.match(["abcccc"])
    assert not pattern.match(["pas"])
    assert not pattern.match(["acbb"])


def test_type_sequence():
    pattern = TypeSequencePattern([str, int, bool])
    assert pattern.match(["hi", 5, True])
    assert not pattern.match(["hi", False, 5])


def test_mixed_sequence():
    pattern = MixedPattern([list, r"abc*", bool])
    assert pattern.match([[1, 2, 3], "abccc", True])
    assert pattern.match([[], "ab", False])
    assert not pattern.match([[], "ab", ""])
    assert not pattern.match([True, "ab", False])
