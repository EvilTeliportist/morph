from morph.languages.languages import *
from morph.testing.tester import Tester
import os
from morph.utils.tools import print_with_colors

from morph.encodings.dependencies.python.modules import *

dependencies = {
    "test_class_extension.morph": {
        Python: {py_mod_Math},
    },
    "test_dependencies.morph": {
        Python: {py_mod_Math},
    },
    "test_elif.morph": {
        Python: {py_mod_Math},
    },
    "test_exceptions.morph": {
        Python: {py_mod_Math},
    },
    "test_operators.morph": {
        Python: {py_mod_Math},
    },
}

errors = {}

directories = [f.path for f in os.scandir("morph/testing/") if f.is_dir() and f.name != "__pycache__"]

print()
for directory in directories[:2]:

    tests_run = 0

    if "bad" in directory:
        print(f"Skipping directory {directory}")
        continue

    print(f"{directory} {'-' * (60 - len(directory))} | ", end="")

    file = directory.split("/")[-1] + ".morph"

    errors[file] = []
    for language in AbstractLanguage.__subclasses__():

        expected_dependencies = set([])

        if file in dependencies.keys():
            if language in dependencies[file].keys():
                expected_dependencies = dependencies[file][language]

        errors[file] += Tester.test(directory + "/" + file, expected_dependencies, language())

    print()

print()
for file in errors.keys():

    if len(errors[file]) > 0:

        print_with_colors(f" Errors for : {file} ".center(60, "-"), color="CVIOLET")

        for error in errors[file]:
            print_with_colors(error, "CRED")

        print()
