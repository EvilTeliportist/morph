## Morph

This application uses a python venv to run Python 3.10. To run files (in development on Windows 10),

- Install [Python 3.10](https://www.python.org/downloads/release/python-3100b1/)
- Duplicate `pyvenv_example.cfg` and name it `pyvenv.cfg`.
- Change the `pyvenv.cfg` to reflect your machine's path.
- Activate the virtual environment with `Scripts/activate`.
- Run `python tests/test310.py` to test if Python 3.10 is working.
- When you're done working, type `deactivate` to close your virtual environment.

To convert your first `.morph` file, run `python morph/engines/driver.py -p morph/testing/test_class/test_class.morph -v 5`.

## How It Works

Here, I'll go through all of the main packages and files, commenting on class structures and various pipelines.

- `morph.engines`: Contains the main lexing files, parsing files, and encoders. It also contains the driver that parses the command line arguments, lexes and parses the file, and triggers the encoder. More information about the lexing and parsing process as well as the encoder can be found in `morph/engines/README.md`.
- `morph.encodings`: Contains the token representation of all support AST (Abstract Syntax Tree) nodes. All nodes **must** be subclasses of the `ASTNode` class in order to be parsed properly by the parser. More information can be found in `morph/encodings/README.md`.
- `morph.languages`: Contains the supported languages as objects. These are useful for keeping data about specific languages and creating new language templates.
- `morph.testing`: Contains test `.morph` files, their correct `Java` and `Python` outputs, as well as a `tester.py` framework.
- `morph.utils`: Contains various utility files that are used throughout the project. Later versions of the project will most likely have some of these become their own packages. Examples include `morph.utils.exceptions` which has package specific exceptions, or `morph.utils.tools` which has generic tool functions.
